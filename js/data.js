// Váriaveis nível global 
var gender;
var doc_name;
var doc_crm;
var pat_name;
var pat_cpf;

// Setar informações do médico
function SetMedicoInfo() {
	
	doc_name = find_element_by_id("med_name");
	doc_crm = find_element_by_id("med_crm");
	
  paginate("2", "3");
}

// Setar informações do paciente
function SetPacienteInfo() {	
	
  pat_name = find_element_by_id("pac_name");
  pat_cpf = find_element_by_id("pac_cpf");

  paginate("3", "4");
}

// Função que retorna valor relativo ao sexo do usuário
function DefineGender(attr){

  gender = find_element_by_name(attr);
  paginate("4", "5");
}








