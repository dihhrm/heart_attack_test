// Closure com funções encapsuladas que realizam a obtenção dos pontos, a adição e subtração e exibe o subtotal no documento
var points = (function addPoints () {

    var total = 0;
    
    return {   
        // Obter o total de pontos
        getTotal: function () { return total },
        plus: function (p)  {
          // Add no total de pontos
          parseInt(p);
          total += p;
        },
        minus: function (p) {
          // Subtrair da pontuação
          parseInt(p);
          total -= p;
        },
        show_points: function(id) { // Exibir os pontos no documento
          document.getElementById(id).innerHTML = total;
        }
    }
})();

// Função para mostrar os valores do resultado no documento
function show_result(total){
  
  var exhibitors = ["r1", "r2", "r3"]; // Id das tags que irão exibir o resultado do teste
  var result = test_result(total); // Resultado do teste
  console.log(result);
  for (i = 0; i < exhibitors.length; i++) {
    for (r = 0; r < result.length; r++) {
      find_and_change_element(exhibitors[i], result[r]);
    }
  }
}

// Função para exibir o resultado final do teste
function test_result(total){

  // Vetor receberá valores em 3 indices relativos aos anos
  var probabilities = [];

  if (gender == 1){

    if (total >= 0 && total <= 35)
      probabilities.push("<0.1%", "<0.4%", "<1%");
    if (total >= 36 && total <= 45)
      probabilities.push("0.1-0.2%", "0.4-1%", "1-3%");
    if (total >= 46 && total <= 55)
      probabilities.push("0.2-0.6%", "1-3%", "3-7%");
    if (total >= 56 && total <= 65)
      probabilities.push("0.6-2%", "3-8%", "7-17%");
    if (total >= 66 && total <= 70)
      probabilities.push("2%", "8-13%", "17-27%");
    if (total >= 71 && total <= 75)
      probabilities.push("2-4%", "13-20%", "27-40%");
    if (total >= 76 && total <= 80)
      probabilities.push("4-6%", "20-30%", "40-56%");   

  } else {

    if (total >= 0 && total <= 60)
      probabilities.push("<0.1%", "<0.4%", "<1%");
    if (total >= 61 && total <= 70)
      probabilities.push("0.1-0.2%", "0.4-1%", "1-3%");
    if (total >= 71 && total <= 80)
      probabilities.push("0.2-0.5%", "1-3%", "3-7%");
    if (total >= 81 && total <= 85)
      probabilities.push("0.5-1%", "3-5%", "7-12%");
    if (total >= 86 && total <= 90)
      probabilities.push("1%", "5-8%", "12-19%");
    if (total >= 91 && total <= 95)
      probabilities.push("1-2%", "8-13%", "19-29%");
    if (total >= 96 && total <= 100)
      probabilities.push("2-4%", "13-20%", "29-43%");    
  }

  return probabilities;
}

// ++++ PAGINAÇÃO DAS TELAS +++

function paginate(current, next){

  // Paginação simples para exibir e ocultar as divs das perguntas 
  current_page = document.getElementById("screen_" + current); // recebe pagina(div/pergunta) atual
  next_page = document.getElementById("screen_" + next); // Pesquisa proxima pagina a ser mostrada
  current_page.setAttribute("style", "display: none;") // esconde a tela atual
  next_page.removeAttribute("style"); // mostra a proxima pagina
}


// +++ FINDERS +++

// Função genérica para realizar pesquisa nos inputs radio
function find_element_by_name(param){
  return document.querySelector("input[name = " + param + "]:checked").value;
}

// Função genérica para pesquisar elementos pelo id
function find_element_by_id(param){
  return document.getElementById(param).value;
}

// Função genérica para pesquisar elementos pelo id
function find_and_change_element(param, new_value){
  return document.getElementById(param).innerHTML = new_value;
}
