// Funções para cada questão
function question_1(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q1");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(2);
  } else if ( selected_option == 'true' ) {
   points.plus(6);
 }

 points.show_points("subtotal");
 paginate("5", "6");
}

function question_2(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q2");
  
  switch (selected_option) {
    case "1": 
      if (gender == 1)     
        points.plus(0);  
      else
        points.plus(2);  
      break;
    case "2":
      if (gender == 1)     
        points.plus(2);       
      else
        points.plus(3);  
      break;
    case "3":
      if (gender == 1)     
        points.plus(4);    
      else
        points.plus(6);  
      break;
  }

  points.show_points("subtotal");
  paginate("6", "7");
}

function question_3(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q3");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(8);
  } else if ( selected_option == 'true' ) {
   points.plus(11);
 }

 points.show_points("subtotal");
 paginate("7", "8");
}

function question_4A(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q4A");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(1);
  } else if ( selected_option == 'true' ) {
   points.plus(4);
 }

 points.show_points("subtotal");
 paginate("8", "9");
}

function question_4B(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q4B");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(2);
  } else if ( selected_option == 'true' ) {
   points.plus(8);
 }

 points.show_points("subtotal");
 paginate("9", "10");
}

function question_4C(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q4C");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(4);
  } else if ( selected_option == 'true' ) {
   points.plus(15);
 }

 points.show_points("subtotal");
 paginate("10", "11");
}

function question_4D(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q4D");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(6);
  } else if ( selected_option == 'true' ) {
   points.plus(18);
 }

 points.show_points("subtotal");
 paginate("11", "12");
}

// Pergunta 05
function question_5(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q5");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(9);
  } else if ( selected_option == 'true' ) {
   points.plus(9);
 }

 points.show_points("subtotal");
 paginate("12", "13");
}

// Pergunta 06
function question_6(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q6");

  if (gender == 1 && selected_option == 'true'){ 
    points.plus(1);
  } else if ( selected_option == 'true' ) {
   points.plus(1);
 }

 points.show_points("subtotal");
 paginate("13", "14");
}

// Pergunta 07
function question_7A(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q7A");

  if (gender == 1 && selected_option == 'true'){ 
    return false;
  } else if ( selected_option == 'true' ) {
   points.minus(5);
 }

 points.show_points("subtotal");
 paginate("14", "15");
}

// Pergunta 07 - B
function question_7B(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q7B");

  if (gender == 1 && selected_option == 'true'){ 
    return false;
  } else if ( selected_option == 'true' ) {
   points.minus(3);
 }

 points.show_points("subtotal");
 paginate("15", "16");
}

// Pergunta 07 - C
function question_7C(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q7C");

  if (gender == 1 && selected_option == 'true'){ 
    return false;
  } else if ( selected_option == 'true' ) {
   points.minus(2);
 }

 points.show_points("subtotal");
 paginate("16", "17");
}

// Pergunta 08
function question_8(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q8");

  if (gender == 1 && selected_option == 'true'){ 
    points.minus(4);
  } else if ( selected_option == 'true' ) {
    points.minus(4);
 }

 points.show_points("subtotal");
 paginate("17", "18");
}


// Pergunta 09
function question_9(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  selected_option = find_element_by_name("q9");

  if (gender == 1 && selected_option == 'true'){ 
    points.minus(4);
  } else if ( selected_option == 'true' ) {
    points.minus(4);
 }
 console.log(points.getTotal());
 points.show_points("subtotal");
 paginate("18", "19");
}


// Pergunta 10
function question_10(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  var value_inserted = parseInt(find_element_by_id("q10"));

  if (gender == 1){ 
    points.plus(value_inserted);
  } else {
    points.plus(value_inserted);
 }
 points.show_points("subtotal");
 paginate("19", "20");
}

// Pergunta 11
function question_11(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  var value_inserted = parseInt(find_element_by_id("q11"));

  if (gender == 1){ 
    points.plus(value_inserted);
  } else {
    points.plus(value_inserted);
 }

 points.show_points("subtotal");
 paginate("20", "21");
}

// Pergunta 12
function question_12A(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  var value_inserted = parseInt(find_element_by_id("q12A"));

  if (gender == 1){ 
    points.plus(value_inserted);
  } else {
    points.plus(value_inserted);
 }

 points.show_points("subtotal");
 paginate("21", "22");
}

// Pergunta 12
function question_12B(){
  
  // Recebe a opção selecionada nos radios, invoca a pesquisa genérica para os inputs passando o id da pergunta
  var value_inserted = parseInt(find_element_by_id("q12B"));

  if (gender == 1){ 
    points.minus(value_inserted);
  } else {
    points.minus(value_inserted);
 }

 show_result(points.getTotal());
 paginate("22", "23");
}